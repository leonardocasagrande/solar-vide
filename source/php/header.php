<!DOCTYPE html>

<html lang="pt_BR">

<head>

  <!-- Google Tag Manager -->
  <script>
    (function(w, d, s, l, i) {
      w[l] = w[l] || [];
      w[l].push({
        'gtm.start': new Date().getTime(),
        event: 'gtm.js'
      });
      var f = d.getElementsByTagName(s)[0],
        j = d.createElement(s),
        dl = l != 'dataLayer' ? '&l=' + l : '';
      j.async = true;
      j.src =
        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
      f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-WWWG8DQ');
  </script>
  <!-- End Google Tag Manager -->

  <meta name="adopt-website-id" content="175a9cc1-15cb-4499-b201-759d91c429b2" />
  <script src="//tag.goadopt.io/injector.js?website_code=175a9cc1-15cb-4499-b201-759d91c429b2" class="adopt-injector"></script>

  <meta charset="UTF-8">

  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <title>

    Solar Vide

  </title>



  <meta name="robots" content="index, follow" />

  <meta name="msapplication-TileColor" content="#ffffff">

  <meta name="theme-color" content="#ffffff">

  <?php wp_head(); ?>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.2/tiny-slider.css">
  <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="<?= get_stylesheet_directory_uri(); ?>/dist/css/style.css">


</head>

<header class="">


  <div class="menu-lg d-none d-lg-flex">

    <a class="btn-home " href="<?= get_site_url(); ?>/">
      <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo.png" alt="">
    </a>

    <a class="menu-link" target="quem-somos">QUEM SOMOS</a>
    <a class="menu-link" target="calculadora">CALCULADORA</a>
    <a class="menu-link" target="portfolio">PORTFÓLIO</a>
    <a class="menu-link" target="parceiros">PARCEIROS</a>
    <a class="menu-link" target="contato">CONTATO</a>

    <a class="btn-seja" href="<?= get_site_url(); ?>/seja-um-representante">SEJA UM REPRESENTANTE</a>

    <div class="midias">

      <a href="https://www.instagram.com/solarvideoficial/" target="_blank">
        <i class="fab fa-instagram"></i>
      </a>

      <a href="https://www.facebook.com/solarvide" target="_blank">
        <i class="fab fa-facebook-f"></i>
      </a>

      <a class="" href="https://www.linkedin.com/company/solar-vide/" target="_blank">
        <i class="fab fa-linkedin-in"></i>
      </a>
    </div>
  </div>




  <nav class=" top-nav   d-lg-none " id="top-nav">

    <a class="logo-closed" href="<?= get_site_url(); ?>">
      <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo.png" alt="">
    </a>

    <a class="logo-opened" href="<?= get_site_url(); ?>">
      <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo-opened.png" alt="">
    </a>

    <div class="hamburger" id="hamburger-1">
      <span class="line"></span>
      <span class="line"></span>
      <span class="line"></span>
    </div>




    <div class="menu">

      <a href="<?= get_site_url(); ?>/quem-somos">QUEM SOMOS</a>
      <a href="#calculadora">CALCULADORA</a>
      <a href="#portfolio">PORTFÓLIO</a>
      <a href="#parceiros">PARCEIROS</a>
      <a href="#contato">CONTATO</a>

      <a class="btn-menu" href="<?= get_site_url(); ?>/seja-um-representante">SEJA UM REPRESENTANTE</a>


    </div>


    <div class="share">


      <div class="col-6  px-0">

        <a href="https://www.instagram.com/solarvideoficial/" target="_blank">
          <i class="fab fa-instagram"></i>
        </a>

        <a target="_blank" href="https://www.facebook.com/solarvide" class="mx-3"><i class="fab fa-facebook-f"></i></a>

        <a href="https://www.linkedin.com/company/solar-vide/" target="_blank">
          <i class="fab fa-linkedin-in"></i>
        </a>

      </div>

    </div>

  </nav>
</header>


<body>

  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WWWG8DQ" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->