<?php get_header('quem'); ?>


<section>
  <div class="banner b-quem">

    <div class="container">
      <h2>Garanta seu lugar ao sol em um dos setores que mais cresce no Brasil e no mundo</h2>

      <a href="#contato" class="btn-cta">seja um representante</a>
    </div>
  </div>


</section>

<!-- <button type="button" data-toggle="modal" data-target="#videoModal" class="modal-video">
  <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/video.png" alt="" class="video d-md-none ">
  <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/video-lg.png" alt="" class="video d-md-block d-none ">
</button> -->

<section id="quem-somos" class="quem-somos quem-page d-lg-flex ">

  <div class="container">

    <div class="title">
      <h2>Quem somos </h2>
      <span></span>
    </div>

    <p>Após trabalhar, conhecer de perto e entender o mercado de geração de energia renovável e limpa, chegamos para revolucioná-lo com excelência e foco na geração de parcerias.</p>

    <p>Nosso maior orgulho está no desenvolvimento pessoal, em compartilhar conhecimento e na oferta de uma qualidade de vida sustentável para nossos clientes, parceiros e para o nosso planeta.</p>

    <a href="#contato" class="btn-cta">fale conosco</a>
  </div>

  <img class="d-none d-lg-block col-lg-6" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo-lg-quem-somos.png" alt="">
</section>

<section class="elgin d-lg-flex flex-row-reverse">
  <div class="container dark-blue-box col-lg-8">

    <img class="elgin-logo" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/elgin-logo.png" alt="elgin">

    <h3><b>Uma parceria que energiza o nosso trabalho.</b></h3>

    <p>Ao longo de sua história de mais de 60 anos, a Elgin estabeleceu um alto padrão de qualidade para o mercado, tornando-se referência em inovação, atendimento e suporte.</p>

    <ul>
      <li>Certificado pelo INMETRO</li>
      <li>Suporte técnico e garantia</li>
      <li>Garantia 100% Nacional</li>
    </ul>
  </div>

  <div class="light-blue-box col-lg-4  ">
    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/elgin-maquina-1.png" alt="Máquina Elgin">
  </div>
</section>

<section id="beneficios" class="beneficios">

  <div class="container">
    <div class="title">
      <h2>Benefícios que só a Solar Vide oferece</h2>
      <span></span>
    </div>

    <div class="beneficios-box">

      <div class="beneficio b-green" data-aos="flip-left">
        <div class="sup">
          <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/beneficio-1.png" alt="">
        </div>

        <div class="content">
          <h3 class="bold">Até 5% de comissão</h3>
          <h4>sobre as vendas</h4>
        </div>
      </div>

      <div class="beneficio b-blue" data-aos="flip-right">
        <div class="sup">
          <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/beneficio-2.png" alt="">
        </div>

        <div class="content">
          <h4>Não é preciso se realocar, a</h4>
          <h3 class="bold">cobertura é nacional</h3>
        </div>
      </div>

      <div class="beneficio b-red" data-aos="flip-left">
        <div class="sup">
          <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/beneficio-3.png" alt="">
        </div>

        <div class="content">
          <h3 class="bold">Capacitação comercial gratuita</h3>
        </div>
      </div>

      <div class="beneficio b-yellow" data-aos="flip-right">
        <div class="sup">
          <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/beneficio-4.png" alt="">
        </div>

        <div class="content">
          <h3 class="bold">Sem investimento inicial</h3>
        </div>
      </div>

    </div>

    <a href="#" target="contato" class="btn-cta btn-orc">faça parte desse time</a>

  </div>

</section>

<section id="porque-trabalhar" class="porque-trabalhar">

  <div class="container">

    <div class="title">
      <h2>Por que trabalhar com energia solar?</h2>
      <span class="d-lg-none"></span>
    </div>

    <div class="blue-box container  ">

      <h4 class="titulo">Porque é uma das indústrias que mais cresce:</h4>

      <div class="item">
        <div class="square">
          <span>01</span>
        </div>

        <p>Em 2019, o mercado cresceu 212% no Brasil.</p>
      </div>

      <div class="item">
        <div class="square">
          <span>02</span>
        </div>

        <p>Apenas no 1º semestre de 2021, o número de módulos fotovoltaicos importados já superou o total do ano de 2020.</p>
      </div>

      <div class="item">
        <div class="square">
          <span>03</span>
        </div>

        <p>O tempo médio de retorno do investimento caiu 10,2% na indústria, 5,9% para residências e 5,4% em aplicações comerciais.</p>
      </div>

      <div class="item">
        <div class="square">
          <span>04</span>
        </div>

        <p>Aumento dos incentivos governamentais.</p>
      </div>

      <div class="item">
        <div class="square">
          <span>05</span>
        </div>

        <p>Encarecimento das fontes de energia tradicionais. </p>
      </div>

      <div class="item">
        <div class="square">
          <span>06</span>
        </div>

        <p>Aumento da preocupação com geração responsável e sustentável de energia</p>
      </div>

      <div class="item">
        <div class="square">
          <span>07</span>
        </div>

        <p>Recorde de investimentos no mercado em 2020: R$ 13 bilhões, e expectativa de R$ 22,6 bilhões em 2021.</p>
      </div>

    </div>

    <img class="center-img" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo-quem.png" alt="">

  </div>

</section>


<section class="depoimentos depo-quem p-lg-0">

  <div class="container">

    <div class="title ">
      <h2>Depoimentos</h2>
    </div>


    <div class="wrapper">


      <?php
      $argsDepoimentos = array(
        'post_type' => 'depoimentos',
        'order' => 'ASC',
      );


      $depoimentos = new WP_Query($argsDepoimentos);


      if ($depoimentos->have_posts()) :; ?>

        <div class="carousel-depoimentos">

          <?php while ($depoimentos->have_posts()) :  $depoimentos->the_post(); ?>


            <div class="item">

              <div class="filtro-wrapper">
                <img class=" d-lg-none avatar col-lg-5 px-0" src="<?= the_post_thumbnail_url(); ?>" alt="">
                <div></div>
              </div>

              <div class="d-flex">

                <div class="filtro-wrapper d-none d-lg-block col-lg-5 px-0">
                  <div class="avatar px-0" style="background: url(<?= the_post_thumbnail_url(); ?>) center center no-repeat"></div>

                  <div></div>
                </div>

                <div class="pl-lg-5 wrapper-content d-flex">
                  <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/aspas.png" alt="" class="aspas">

                  <div class="content ">
                    <p><?= the_content(); ?></p>

                    <h4 class="name"><?= the_title(); ?></h4>
                    <h4 class="sub"><?= the_field('profissao'); ?></h4>
                  </div>
                </div>
              </div>
            </div>

          <?php endwhile; ?>


        </div>

      <?php endif; ?>
    </div>

  </div>
</section>

<?php get_footer(); ?>