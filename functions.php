<?php

add_filter('show_admin_bar', '__return_false');

add_theme_support( 'post-thumbnails' );

function cptui_register_my_cpts() {

	/**
	 * Post Type: Portfólio.
	 */

	$labels = [
		"name" => __( "Portfólio", "custom-post-type-ui" ),
		"singular_name" => __( "Portfólio", "custom-post-type-ui" ),
	];

	$args = [
		"label" => __( "Portfólio", "custom-post-type-ui" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => [ "slug" => "portfolio", "with_front" => true ],
		"query_var" => true,
		"supports" => [ "title" ],
		"show_in_graphql" => false,
	];

	register_post_type( "portfolio", $args );

	/**
	 * Post Type: Parceiros.
	 */

	$labels = [
		"name" => __( "Parceiros", "custom-post-type-ui" ),
		"singular_name" => __( "Parceiro", "custom-post-type-ui" ),
	];

	$args = [
		"label" => __( "Parceiros", "custom-post-type-ui" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => [ "slug" => "parceiros", "with_front" => true ],
		"query_var" => true,
		"supports" => [ "title" ],
		"show_in_graphql" => false,
	];

	register_post_type( "parceiros", $args );

	/**
	 * Post Type: Depoimentos.
	 */

	$labels = [
		"name" => __( "Depoimentos", "custom-post-type-ui" ),
		"singular_name" => __( "Depoimento", "custom-post-type-ui" ),
	];

	$args = [
		"label" => __( "Depoimentos", "custom-post-type-ui" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => [ "slug" => "depoimentos", "with_front" => true ],
		"query_var" => true,
		"supports" => [ "title", "editor", "thumbnail" ],
		"show_in_graphql" => false,
	];

	register_post_type( "depoimentos", $args );
}

add_action( 'init', 'cptui_register_my_cpts' );


?>