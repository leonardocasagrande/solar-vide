<?php get_header();

$paged = (isset($_GET['pagina']) ? $_GET['pagina'] : 1);


$new_limit = 9;


$iphone = strpos($_SERVER['HTTP_USER_AGENT'], "iPhone");
$android = strpos($_SERVER['HTTP_USER_AGENT'], "Android");

$berry = strpos($_SERVER['HTTP_USER_AGENT'], "BlackBerry");
$ipod = strpos($_SERVER['HTTP_USER_AGENT'], "iPod");

if ($iphone || $android ||  $ipod || $berry == true) {
  $new_limit = 3;
}

?> <section><div class="banner"><div class="container"><h2>As melhores soluções em energia fotovoltaica</h2><a href="#calculadora" class="btn-cta">simule meu projeto</a></div></div><div class="seja-representante d-lg-none"><h4>seja um representante</h4></div></section><section class="elgin d-lg-flex flex-row-reverse"><div class="container dark-blue-box col-lg-8"><img class="elgin-logo" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/elgin-logo.png" alt="elgin"><h3><b>Uma parceria que energiza o nosso trabalho.</b></h3><p>Ao longo de sua história de mais de 60 anos, a Elgin estabeleceu um alto padrão de qualidade para o mercado, tornando-se referência em inovação, atendimento e suporte.</p><ul><li>Certificado pelo INMETRO</li><li>Suporte técnico e garantia</li><li>Garantia 100% Nacional</li></ul></div><div class="light-blue-box col-lg-4"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/elgin-maquina-1.png" alt="Máquina Elgin"></div></section><section id="quem-somos" class="quem-somos d-lg-flex"><div class="container"><div class="title"><h2>Quem somos</h2><span></span></div><p>Após trabalhar, conhecer de perto e entender o mercado de geração de energia renovável e limpa, chegamos para revolucioná-lo com excelência e foco na geração de parcerias.</p><p>Nosso maior orgulho está no desenvolvimento pessoal, em compartilhar conhecimento e na oferta de uma qualidade de vida sustentável para nossos clientes, parceiros e para o nosso planeta.</p><a href="#contato" class="btn-cta">fale conosco</a></div><img class="d-none d-lg-block col-lg-6" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo-lg-quem-somos.png" alt=""></section><section id="calculadora" class="calculadora py-lg-0 d-lg-flex"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/descubra-lg.png" alt="" class="d-none d-lg-block col-lg-5 pl-0"><div class="container py-lg-3 px-lg-5 d-lg-flex flex-column justify-content-center"><!-- <div class="resultado">
        <div class="detail green"></div>
        <div>
          <h4>Custo total do projeto:</h4>
          <h3><span id="custoTotal"></span>*</h3>
<div class="obs"><small>*Parcelamento em até 72x com as melhores taxas do mercado</small></div>
        </div>
</div>
      --> <?= do_shortcode('[contact-form-7 id="141" title="Calculadora Dudu Rocha"]'); ?> </div></section><section class="onde-porque-usar"><div class="container onde-usar"><div class="title pt-lg-5"><h2>Onde usar energia solar</h2><span></span></div><div class="collapse-box d-lg-flex"><div class="item d-lg-block col-lg-4" data-aos="fade-up" data-aos-delay="300"><a class="lg-prevent" data-toggle="collapse" href="#multiCollapseExample1" role="button" aria-expanded="true" aria-controls="multiCollapseExample1"><img class="bg" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/collapse-1.png" alt=""> <span class="number">01</span> <span class="titulo">Residências</span> <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/arrow-yellow.png" alt="" class="arrow arrow-down"></a><div class="collapse show" id="multiCollapseExample1"><div class="">Além da economia no valor da conta de luz, que pode variar de 50% a 95%, as placas de energia solar requerem pouquíssima manutenção, o sistema pode ser ampliado caso haja espaço e ainda não ocupam espaço útil do imóvel.</div></div></div><div class="item d-lg-block col-lg-4" data-aos="fade-up" data-aos-delay="500"><a class="lg-prevent" data-toggle="collapse" href="#multiCollapseExample2" role="button" aria-expanded="false" aria-controls="multiCollapseExample2"><img class="bg" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/collapse-2.png" alt=""> <span class="number">02</span> <span class="titulo">Indústrias</span> <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/arrow-yellow.png" alt="" class="arrow"></a><div class="collapse" id="multiCollapseExample2"><div class="">Independente do tamanho da sua empresa, plantas comerciais e industriais se tornam muito mais econômicas, modernas e com apelo sustentável utilizando energia fotovoltaica.</div></div></div><div class="item d-lg-block col-lg-4" data-aos="fade-up" data-aos-delay="700"><a class="lg-prevent" data-toggle="collapse" href="#multiCollapseExample3" role="button" aria-expanded="false" aria-controls="multiCollapseExample3"><img class="bg" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/collapse-3.png" alt=""> <span class="number">03</span> <span class="titulo">Rural</span> <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/arrow-yellow.png" alt="" class="arrow"></a><div class="collapse" id="multiCollapseExample3"><div class="">Economia, produtividade e segurança energética, especialmente em regiões onde a energia produzida pelas hidrelétricas é inviável.</div></div></div></div></div><div class="porque-usar container"><div class="title"><h2>Por que investir em energia fotovoltaica?</h2><span></span></div><div class="wrapper"><div class="carousel-porque d-lg-flex flex-wrap"><div class="item col-lg-6"><div class="item-wrapper"><div class="circle"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/porque-1.png" alt=""></div><div><h4 class="titulo">Economia na sua conta de luz</h4><p>Toda energia que é gerada é reaproveitada e volta para você.</p></div></div></div><div class="item col-lg-6"><div class="item-wrapper"><div class="circle"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/porque-2.png" alt=""></div><div><h4 class="titulo">Seu imovél<br>valorizado</h4><p>Investimento que se paga no médio prazo e valoriza o seu imóvel.</p></div></div></div><div class="item col-lg-6"><div class="item-wrapper"><div class="circle"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/porque-3.png" alt=""></div><div><h4 class="titulo">Energia<br>sustentável</h4><p>Energia renovável e não poluente, como pode e deve ser.</p></div></div></div><div class="item col-lg-6"><div class="item-wrapper"><div class="circle"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/porque-4.png" alt=""></div><div><h4 class="titulo">Diferencial de mercado para empresas</h4><p>Atrair parceiros que valorizam a sustentabilidade.</p></div></div></div><div class="item col-lg-6"><div class="item-wrapper"><div class="circle"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/porque-5.png" alt=""></div><div><h4 class="titulo">Projetos<br>personalizados</h4><p>Profissionais capacitados que compreendem as necessidades específicas de cada cliente e estrutura para atendê-las.</p></div></div></div><div class="item col-lg-6"><div class="item-wrapper"><div class="circle"><img class="elgin-porque" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/elgin-logo.png" alt=""></div><div><h4 class="titulo">Parceria com empresa referência</h4><p>Nossa parceria com a Elgin proporciona suporte especializado em todo o território nacional.</p></div></div></div></div></div></div><div class="yellow-box"><div class="container d-lg-flex justify-content-between align-items-center"><div><div class="title mb-lg-0"><h2>Experiência comprovada</h2></div><p class="my-lg-0">Mais de <b>10</b> anos de mercado.</p></div><a target="contato" class="btn-cta btn-orc bg-blue">solicite um orçamento</a></div></div></section><section id="portfolio" class="portfolio"><div class="container"><div class="title"><h2>Portfólio</h2><span></span></div></div><div class="light-blue-box"> <?php
    $argsPortfolio = array(
      'post_type' => 'portfolio',
      'order' => 'DESC',
      'posts_per_page' => $new_limit,
      'paged' => $paged,



    );
    $portfolio = new WP_Query($argsPortfolio);

    $contador = 0;

    if ($portfolio->have_posts()) :; ?> <div class="container project-box d-md-flex flex-wrap justify-content-between"> <?php while ($portfolio->have_posts()) :  $portfolio->the_post(); ?> <button class="projeto" type="button" data-toggle="modal" data-target="#exampleModal-<?= $contador; ?>"><div class="img-projeto" style="background: url(<?= the_field('imagem'); ?>);"></div></button> <?php $contador++;
        endwhile; ?> </div> <?php else : ?> <p class="response-p">Não encontramos nenhum projeto.</p> <?php endif; ?> </div><div class="barradenavegacao"> <?php


    echo paginate_links(array(
      'format' =>
      '?pagina=%#%', 'show_all' => false, 'current' => max(1, $paged), 'total' => $portfolio->max_num_pages, 'prev_text' => '<i class="fas fa-caret-left fa-2x"></i>', 'next_text' => '<i class="fas fa-caret-right fa-2x"></i>',
      'type' => 'list'
    ));
    ?> </div><div class="container"><a href="#" target="contato" class="btn-cta btn-orc">solicite um orçamento</a></div></section><!-- <button type="button" data-toggle="modal" data-target="#videoModal" class="modal-video">
  <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/video.png" alt="" class="video d-md-none ">
  <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/video-lg.png" alt="" class="video d-md-block d-none ">
</button> --><section class="depoimentos margin-top-custom p-lg-0"><div class="container"><div class="title"><h2>Depoimentos</h2></div><div class="wrapper"> <?php
      $argsDepoimentos = array(
        'post_type' => 'depoimentos',

      );


      $depoimentos = new WP_Query($argsDepoimentos);


      if ($depoimentos->have_posts()) :; ?> <div class="carousel-depoimentos"> <?php while ($depoimentos->have_posts()) :  $depoimentos->the_post(); ?> <div class="item"><div class="filtro-wrapper"><img class="d-lg-none avatar col-lg-5 px-0" src="<?= the_post_thumbnail_url(); ?>" alt=""><div></div></div><div class="d-flex"><div class="filtro-wrapper d-none d-lg-block col-lg-5 px-0"><div class="avatar px-0" style="background: url(<?= the_post_thumbnail_url(); ?>) center center no-repeat"></div><div></div></div><div class="pl-lg-5 wrapper-content d-flex"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/aspas.png" alt="" class="aspas"><div class="content"><p><?= the_content(); ?></p><h4 class="name"><?= the_title(); ?></h4><h4 class="sub"><?= the_field('profissao'); ?></h4></div></div></div></div> <?php endwhile; ?> </div> <?php endif; ?> </div></div></section><section id="parceiros" class="parceiros"><div class="container"><div class="d-lg-flex align-items-center"><div class="wrapper col-lg-6 px-0"><div class="title"><h2>Parceiros</h2><span></span></div> <?php
        $argsParceiros = array(
          'post_type' => 'parceiros',
          'order' => 'ASC',
          'posts_per_page' => -1
        );


        $parceiros = new WP_Query($argsParceiros);
        $totalPosts = $parceiros->found_posts;

        $contador = 1;

        if ($parceiros->have_posts()) :; ?> <div class="carousel-parceiros"> <?php while ($parceiros->have_posts()) :  $parceiros->the_post(); ?> <?php $currentPost = get_the_ID(); ?> <?php if ($currentPost == 108 || $currentPost == 117) : ?> <div class="item"><div class="px-lg-2 justify-content-start"><img src="<?= the_field('logo') ?>" alt=""></div></div> <?php else : ?> <?php if ($contador % 2 == 1) : ?> <div class="item"><div class="px-lg-2"><img src="<?= the_field('logo') ?>" alt=""> <?php endif; ?> <?php if ($contador % 2 == 0) : ?> <img src="<?= the_field('logo') ?>" alt=""> <?php endif; ?> <?php if ($contador % 2 == 0 || $contador == $totalPosts) : ?> </div></div> <?php endif; ?> <?php endif; ?> <?php $contador++;
            endwhile; ?> </div> <?php endif; ?> </div><img class="big-logo d-none d-lg-block col-lg-5" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/solar-big.png" alt=""></div></div><a target="calculadora" class="simulacao"><h3>Faça uma simulação</h3><i class="fa fa-angle-right"></i></a></section> <?php get_footer(); ?>