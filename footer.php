<?php if (is_page('seja-um-representante')) : $d_none = ' d-none ';
  $d_block = ' d-block ';
  $custom_orcamento = ' custom-orc ';
else : $d_none = ' d-block';
  $d_block = ' d-none';
endif;

$paged = (isset($_GET['pagina']) ? $_GET['pagina'] : 1);


$new_limit = 9;


$iphone = strpos($_SERVER['HTTP_USER_AGENT'], "iPhone");
$android = strpos($_SERVER['HTTP_USER_AGENT'], "Android");

$berry = strpos($_SERVER['HTTP_USER_AGENT'], "BlackBerry");
$ipod = strpos($_SERVER['HTTP_USER_AGENT'], "iPod");

if ($iphone || $android ||  $ipod || $berry == true) {
  $new_limit = 3;
}

if (is_page('obrigado')) {
  $orc_none = ' d-none';
}

?> <section id="contato" class="orcamento <?= $custom_orcamento,
                                        $orc_none; ?>"><div class="container d-lg-flex"><div class="col-lg-5"><h4 class="titulo <?= $d_none; ?>">Orçamento</h4><span class="sub <?= $d_none; ?> px-0">Faça como outras milhares de pessoas, invista em um mundo melhor!</span> <span class="sub <?= $d_block; ?> px-0"><b>Faça parte do nosso time.<br>Preencha o formulário abaixo e aguarde o nosso contato.</b></span></div><div class="form-box"> <?php if(is_page('dudurocha')):  echo do_shortcode('[contact-form-7 id="143" title="Orçamento Dudu Rocha"]'); elseif (!is_page('seja-um-representante')) : echo do_shortcode('[contact-form-7 id="47" title="Orçamento"]'); 
      else : echo do_shortcode('[contact-form-7 id="48" title="Trabalhe-conosco"]');
      endif; ?> </div></div></section><footer><div class="container"><img class="logo-vertical" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo-footer.png" alt=""><div class="wrapper-total col-lg-7"><div class="col-lg-6 px-0 pr-lg-4"><div class="wrapper-link"><i class="fas fa-map-marker-alt"></i> <a href="https://www.google.com.br/maps/place/R.+Anton%C3%8Do+de+Carvalho,+110+-+Centro,+Sumar%C3%A9+-+SP,+13170-032/@-22.8190925,-47.2732029,17z/data=!3m1!4b1!4m5!3m4!1s0x94c8bd69b2818aeb:0xd31df43c1a7223e4!8m2!3d-22.8190975!4d-47.2710142" target="_blank">R. Antônio de Carvalho, 110, Sala 06, Centro - Sumaré CEP: 13170-032</a></div><div class="wrapper-link mt-lg-4"><i class="fab fa-whatsapp"></i> <a href="https://api.whatsapp.com/send?phone=5519989802992">19 98980-2992</a></div></div><div class="custom-width"><h5><b>Siga-nos<br>nas redes sociais</b></h5><div class="wrapper-link"><i class="fab fa-instagram"></i> <a href="https://www.instagram.com/solarvideoficial/" target="_blank">@solarvideoficial</a></div><div class="wrapper-link"><i class="fab fa-facebook-f"></i> <a href="https://www.facebook.com/solarvide" target="_blank">/solarvideenergia</a></div><div class="wrapper-link"><i class="fab fa-linkedin-in"></i> <a href="https://www.linkedin.com/company/solar-vide/" target="_blank">Solar Vide</a></div></div></div></div><img class="house" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/house.png" alt=""></footer><div class="btn-zapzap d-none d-lg-block"><a id="btn-whats-app" target="_blank" href="https://api.whatsapp.com/send?phone=5519989802992"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/btn-zap.png" alt="Chama no zap"></a></div> <?php
$argsPortfolio = array(
  'post_type' => 'portfolio',
  'order' => 'DESC',
  'posts_per_page' => $new_limit,
  'paged' => $paged,
);

$portfolio = new WP_Query($argsPortfolio);
$contador = 0;

if ($portfolio->have_posts()) :;

  while ($portfolio->have_posts()) :  $portfolio->the_post(); ?> <div class="modal modal-port fade" id="exampleModal-<?= $contador; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"><div class="wrapper"><div class="modal-dialog" role="document"><div class="modal-content"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><div class="modal-body"><img src="<?= the_field('imagem'); ?>" alt=""><div class="filtro"></div><h3><?= the_field('kwp'); ?> kWp</h3><p><?= the_field('descricao'); ?></p></div></div></div></div></div> <?php $contador++;
  endwhile;
endif; ?> <div class="modal modal-port fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="videoModalLabel" aria-hidden="true"><div class="wrapper"><div class="modal-dialog" role="document"><div class="modal-content"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><div class="modal-body"><iframe src="https://player.vimeo.com/video/349989295?background=1&amp;autoplay=1&amp;loop=1&amp;byline=0&amp;title=0&amp;autopause=0&amp;muted=1" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" frameborder="0"></iframe></div></div></div></div></div> <?php wp_footer(); ?> <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.2/min/tiny-slider.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script><script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.3/jquery.inputmask.min.js"></script><script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.js"></script><script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script><script src="<?php echo get_stylesheet_directory_uri(); ?>/dist/js/app.js"></script><script>AOS.init();</script>